# Use an official Ubuntu runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages
RUN apt update && apt install -y \
	autoconf \
	autotools-dev \
	cmake \
	gettext \
	git \
	g++ \
	ipython3 \
	libgoogle-glog-dev \
	libatlas-base-dev \
	libboost-dev \
	libgsl-dev \
	libgtest-dev \
	libtool \
	make \
	mercurial \
	python3 \
	python3-pip \
	python3-setuptools \
	python3-matplotlib \
	python3-pandas \
	python3-scipy \
	python3-numpy \
	scons

RUN apt update && apt install -y --no-install-recommends \
	texlive-latex-base \
	texlive-latex-extra \
	texlive-pictures

RUN pip3 install pymc3 'cython<=0.27.1'

# Clone and compile repositories
RUN python3 download-install.py

# Run experiment-plot.py when the container launches
CMD ["python3", "experiment-plot.py"]
