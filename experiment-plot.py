import subprocess
import os
import shutil

##########################################################
################### Dynamic Comparison ###################
##########################################################

# Parameters
nlist = [2**x for x in range(10,30)]
C=-1
alpha = 0.75
T=0.1

networkitDir = "networkit-hyperbolic-kd/"

# Benchmark Implementation of Blaesius et al.
print("Starting experiments with generator of Blaesius et al.")
os.chdir('girg-benchmark')
optionList = ['-T', str(T), '-alpha', str(alpha), '-C', str(C), '-terse', '-iterations', str(10000)]
girgString = ""
girgEnv = os.environ.copy()
girgEnv["GLOG_minloglevel"] = str(3)
for n in nlist:
    name = 'test-'+str(n)
    girgString += subprocess.run(['./embedder', '-n', str(n), '-generate', str(name)] + optionList, stdout=subprocess.PIPE, env=girgEnv).stdout.decode()
os.chdir('..')
print(" ... Done.")


# Write results in tab-separated text file
with open('kromer-dynamic-log.dat', 'w') as f:
    f.write('seed\tn\tgen\tavgDegree\titer\tdyn\n')
    f.write(girgString)

# Move generated graphs so they can be used by our generator
for n in nlist:
    graphname = "test-"+str(n)+"-links.txt"
    coordname = "test-"+str(n)+"-coordinates.txt"
    if not os.path.exists(networkitDir + graphname):
        shutil.move("girg-benchmark/" + graphname, networkitDir + graphname)
    if not os.path.exists(networkitDir + coordname):
        shutil.move("girg-benchmark/" + coordname, networkitDir + coordname)

# Benchmark different balance parameters
print("Starting Quadtree balance benchmark.")
os.chdir("networkit-hyperbolic-kd")
balanceString = subprocess.run(['./NetworKit-Tests-Opt', '--gtest_filter=*benchmarkQuadTreeBalance'], stderr=subprocess.PIPE).stderr
print(" ... Done.")

# Write results
with open('../pnq-benchmark-balance.dat', 'w') as f:
    f.write(balanceString.decode())

# Benchmark dynamic movement with externally generated graphs
print("Starting dynamic movement benchmark.")
timeString = subprocess.run(['./NetworKit-Tests-Opt', '--gtest_filter=*benchmarkExternalEmbedderCall'], stderr=subprocess.PIPE).stderr
print(" ... Done.")

# Write results
with open('../pnq-dynamic-log-average.dat', 'w') as f:
    f.write(timeString.decode())


# Get query count depending on cell radius
print("Starting Query Count Experiment")
queryString = subprocess.run(['./NetworKit-Tests-Opt', '--gtest_filter=*benchmarkQueryCount', '--loglevel=INFO'], stdout=subprocess.PIPE).stdout
print(" ... Done.")

# Parse Output
queryList = queryString.decode().split('\n')
cellList = []
for row in queryList:
    if not 'Leaf' in row:
        continue
    elems = row.split(' ')
    cellList.append([float(elems[-4].split(',')[1].split(')')[0]), int(elems[-2])])

# Sort output into annuli
thresholds = [i/2 for i in range(26, 36)]# [13, 14, 15, 16, 17, 18]
numCells = [0 for i in thresholds]
numVisits = [0 for i in thresholds]
for cell in cellList:
    thresholdIndex = 0
    while cell[0] > thresholds[thresholdIndex]:
        thresholdIndex += 1
    numCells[thresholdIndex] += 1
    numVisits[thresholdIndex] += cell[1]

# Write results
with open('../quadtree-cell-query-count.dat', 'w') as f:
    f.write('radius\tqueries\n')
    for i in range(len(thresholds)):
        f.write(str(thresholds[i]) + '\t' + str(float(numVisits[i])/numCells[i]) + '\n')

# Combine time measurements for tradeoff calculation
girgList = girgString.split('\n')
nativedyn = []
for row in girgList:
    elems = row.split('\t')
    if len(elems) > 5:
        nativedyn.append(elems[5])

i = 0
rows = timeString.decode().split('\n')
with open('../pnq-dynamic-combined.dat', 'w') as f:
    f.write(rows[0]+'\n')
    f.write(rows[1] + '\tnativedyn' + '\n')
    for i in range(2,len(rows)):
        row = rows[i]
        if len(nativedyn) < i:
            nativedyn.append('')
        f.write(row + '\t' + str(nativedyn[i-2]) + '\n')
        i += 1

os.chdir("..")

##########################################################
############# Static generation benchmarks ###############
##########################################################
temperatures = ['cold', 'lukewarm']

os.chdir('girg-benchmark')
for t in temperatures:
    scriptname = 'experiments-girg-' + t + '-time.py'
    shutil.copy('../scripts/' + scriptname, '.')
    subprocess.run(['python3', scriptname])
    shutil.copy('girg-' + t + '-time.dat', '..')
os.chdir("..")

os.chdir('Hyperbolic-Graph-Generator')
for t in temperatures:
    scriptname = 'experiments-native-' + t + '-time.py'
    shutil.copy('../scripts/' + scriptname, '.')
    subprocess.run(['python3', scriptname], env={'LD_LIBRARY_PATH' : 'install/lib64'})
    shutil.copy('aldecoa-' + t + '-time.dat', '..')
os.chdir("..")

os.chdir('networkit-general-polylog')
for t in temperatures:
    scriptname = 'experiments-networkit-' + t + '-time.py'
    shutil.copy('../scripts/' + scriptname, '.')
    subprocess.run(['python3', scriptname], env={'LD_LIBRARY_PATH' : 'install/lib64'})
    shutil.copy('polylog-' + t + '-time.dat', '..')
os.chdir("..")

os.chdir('networkit-hyperbolic-legacy')
for t in temperatures:
    scriptname = 'experiments-quadtree-' + t + '-time.py'
    shutil.copy('../scripts/' + scriptname, '.')
    subprocess.run(['python3', scriptname], env={'LD_LIBRARY_PATH' : 'install/lib64'})
    shutil.copy('quadtree-' + t + '-time.dat', '..')
os.chdir("..")

###################################
# Distributional property studies #
###################################
os.chdir('Hyperbolic-Graph-Generator')
scriptname = 'experiments-native-temperature-repetitions.py'
shutil.copy('../scripts/' + scriptname, '.')
subprocess.run(['python3', scriptname], env={'LD_LIBRARY_PATH' : 'install/lib64'})
outname = 'properties-native-repetitions.dat'
shutil.copy(outname, '..')
os.chdir("..")

os.chdir('networkit-general-polylog')
scriptname = 'experiments-temperature-repetitions.py'
shutil.copy('../scripts/' + scriptname, '.')
subprocess.run(['python3', scriptname])
outname = 'properties-bandgen-repetitions.dat'
shutil.copy(outname, '..')
os.chdir("..")

# aggregate logs of network properties into mean and variance statistics
subprocess.run(['python3', 'scripts/Aggregate-Network-Property-Stats-Warming-RHGs.py'])
# compute normalized differences used in plots
subprocess.run(['python3', 'scripts/Difference-Network-Properties-Warming-RHGs.py'])
# compute Bayes factor of hypotheses regarding differences
subprocess.run(['python3', 'scripts/Equivalence-BF-Hyperbolic-Generator.py'])

#######################################################
##################### Plot results ####################
#######################################################

def compilePlot(basename):
    texfile = basename + '.tex'
    result = subprocess.call(["pdflatex", '-interaction=nonstopmode', texfile],stdout=subprocess.DEVNULL)
    logfile = basename+'.log'
    auxfile = basename+'.aux'
    pdffile = basename+'.pdf'
    if result == 0:
        print('Compilation successful! Open ', pdffile, ' to see results.',sep='')
        subprocess.call(["rm", logfile])
    else:
        print('Compilation failed. Send ', texfile, ' and ', logfile, ' to Moritz and ask what went wrong.')
    # clean up
    subprocess.call(["rm", auxfile])

compilePlot('plot-benchmark-balance')
compilePlot('plot-query-count')
compilePlot('plot-running-times')
compilePlot('plot-construction-tradeoff')
compilePlot('plot-cold-static')
compilePlot('plot-lukewarm-static')

shutil.copy("native-stats.dat", "native-comparison")
shutil.copy("band-stats.dat", "native-comparison")
shutil.copy("relativeDiff.dat", "native-comparison")
os.chdir("native-comparison")
for name in ["cc", "degass", "degen", "diameter"]:
    for plotType in ["native", "diff"]:
        compilePlot(plotType + "-plot-" + name)