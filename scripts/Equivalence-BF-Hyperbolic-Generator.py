import pymc3 as pm
import numpy as np
import random
import math

from scipy import optimize

def readFile(filename):
    result = dict()
    targetNumCols = 7
    with open(filename, 'r') as f:
        for line in f:
            elems = line.split()
            floats = [float(elem) for elem in elems]
            # skip lines with wrong number of columns
            if not len(floats)-3 == targetNumCols:
                print("Malformed line: " + line)
                continue
            
            # first three columns are experiment parameters: T, gamma, k
            keytuple = (floats[0], floats[1], floats[2])
            if not keytuple in result:
                result[keytuple] = []
                
            result[keytuple].append(floats[3:])
    return result


def computeBF(nativeDict, compDict, threshold = 0.1):
    colNames = ["avgDegree", "Diameter", "cc", "degen", "degAss"]
    numCols = len(colNames)

    # only compare measurements for the same set of keys
    commonKeys = nativeDict.keys() & compDict.keys()

    # Pair measurements for same input parameters, take their relative difference
    # Assumption: Normally distributed errors, so the differences follow a normal distribution
    differences = []
    for key in sorted(commonKeys):
        numSamples = min(len(nativeDict[key]), len(compDict[key]))
        for i in range(numSamples):
            diff = np.divide(np.subtract(nativeDict[key][i], compDict[key][i]), nativeDict[key][i])
            # We do not take all columns, as the last two were from methods with questionable reliability
            differences.append(diff[:numCols])

    numProps = numCols
    numKeys = len(commonKeys)

    # ## Define statistical model

    # We (somewhat arbitrarily) say that the two generators are equivalent in practice
    # if the difference between their average results is one order of magnitude less than the variability
    # _within_ the results of each generator. Thus 0.1.

    basic_model = pm.Model()

    with basic_model:
        pi = (0.999, 0.001)
        assert(sum(pi) == 1)

        # Indicator variable for which model is selected: 1 is for equivalence, 0 for difference
        selected_model = pm.Bernoulli('selected_model', p=pi[1])

        # Prior uniform distribution for the mean normalized difference in equivalence hypothesis
        # Support on [0, 0.1], one variable per property.
        b = pm.Uniform('b', lower=0, upper=threshold, shape=(numProps))
        
        # Prior distribution for mean normalized difference in alternate hypothesis
        # Support on [0.1, infinity], one variable per property.
        c = pm.Bound(pm.Exponential, lower=threshold)('c', lam=1, shape=(numProps))
        
        # Variance of results. Unknown, thus included as free variable.
        # One variable per property.
        sigma = pm.InverseGamma('sigma', alpha=1,beta=1, shape=(numProps))

        # Direction of difference. Unknown, thus included as free variable.
        # One variable per property.
        sign = pm.Bernoulli('sign', p=0.5, shape=(numProps))

        # mean of distribution of differences. 
        # Deterministic variable depending on sign, selected_model, b, c and sigma
        mu = (1-2*sign)*(selected_model*(b)+(1-selected_model)*c)*sigma

        # Observed differences
        Y_obs = pm.Normal('Y_obs', mu=mu, sd=math.sqrt(2)*sigma, observed=np.array(differences), shape=(numProps))


    # ## Use MCMC for inference
    with basic_model:
        # draw 10000 posterior samples
        trace = pm.sample(10000, tune=1000)

    # ## Compute Bayes Factor
    # Bayes factor is the posterior odds ratio divided by the prior odds ratio.
    BF = ((1-pm.summary(trace)['mean']['selected_model'])/pm.summary(trace)['mean']['selected_model']) / (pi[0]/pi[1])

    # In this model, a Bayes factor below 0.1 supports the hypothesis that both distributions are equivalent,
    # a Bayes factor above 10 supports the opposite hypothesis. Further away from 1 means stronger support.
    # If a Bayes factor is 0 or infinity, it is beyond the precision achievable with the chosen 10000 samples.
    return BF

if __name__ == "__main__":
    nativeFile = "properties-native-repetitions.dat"
    compFile = "properties-bandgen-repetitions.dat"

    nativeDict  = readFile(nativeFile)
    compDict = readFile(compFile)

    bf = computeBF(nativeDict, compDict)
    with open('equivalence-bayes-factor.txt', 'w') as f:
        f.write("Computed Bayes factor: " +  str(bf))
    