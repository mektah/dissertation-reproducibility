import time
import math
import geometric
import os
import subprocess

p = 1

nList = [2**i for i in range(10, 30)]
gamma = 3
k = 8
T = 0.5

filename = 'girg-lukewarm-time.dat'

with open(filename, 'w') as f:
	f.write('% T:' + str(T) + ' gamma:' + str(gamma) + ' k:' + str(k) + '\n')
	f.write('% Threads:' + str(p) + '\n')
	f.write('n\ttime\n')

for n in nList:
	with open(filename, 'a') as f:
		alpha = (gamma-1)/2
		C = geometric.getC(n, k, alpha)*(1)
		optionList = ['-T', str(T), '-alpha', str(alpha), '-C', str(C), '-dontsave']
		girgEnv = os.environ.copy()
		girgEnv["GLOG_minloglevel"] = str(3)
		name = 'test-'+str(n)
		print("Starting run with", n, " nodes, gamma ", gamma, ", k ", k, ", T ", T, " and C ", C)
		start = time.time()
		resultstring = subprocess.run(['./embedder', '-n', str(n), '-generate', str(name)] + optionList, stdout=subprocess.PIPE, env=girgEnv).stdout.decode()
		end = time.time()
		f.write(str(n)  + '\t' + str(end-start) + '\n')


		print("Finished run in ", end - start, " seconds:")
		print(resultstring)
