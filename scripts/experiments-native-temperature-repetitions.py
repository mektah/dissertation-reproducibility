#! /usr/bin/env python3.4
import os
import sys

owndir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(owndir, "../networkit-general-polylog"))

import networkit
import time
import subprocess

def readHGGraph(path):
    lines = []
    with open(path, 'r') as f:
        for line in f:
            lines.append(line)

    paramlist = lines[0].split('\t')
    n = int(paramlist[1])

    G = networkit.Graph(n)

    for i in range(n+1,len(lines)):
        edgelist = lines[i].split('\t')
        firstNode = int(edgelist[0])-1
        secondNode = int(edgelist[1])-1
        assert(firstNode >= 0)
        assert(firstNode < n)
        assert(secondNode >= 0)
        assert(secondNode < n)
        G.addEdge(firstNode, secondNode)
    
    return G

def generateHGGraph(n=1000, k=10, gamma=2, T=0, seed=1, binary='hyperbolic_graph_generator'):
    subprocess.call([binary, '-n', str(n), '-g', str(gamma), '-k', str(k), '-t', str(T), '-s', str(seed)])
    G = readHGGraph('graph.hg')
    assert(G.numberOfNodes() == n)
    subprocess.call(['rm', 'graph.hg'])
    return G

n = 10**4
repetitions = 250
gammalist = [float(x)/10 for x in range(22, 60, 6)]
gammalist.reverse()
klist = [2**x for x in range(2,9,2)]
Tlist = [0] #[float(x)/10 for x in range(0,10,3)]
nlist = [10**x for x in range(4,5)]

with open("properties-native-repetitions.dat", 'a') as f:
	for i in range(repetitions):
		for T in Tlist:
			for gamma in gammalist:
				for k in klist:
					print("Starting run with", n, " nodes, gamma ", gamma, ", k ", k, " and T ", T)
					start = time.time()
					G = generateHGGraph(n, k, gamma, T, start, 'install/bin/hyperbolic_graph_generator')
					end = time.time()
					print("g",end='',flush=True)
					
					m = G.numberOfEdges()
					diam = networkit.distance.Diameter(G).run().getDiameter()[0]
					print("d",end='',flush=True)
					lcc = networkit.centrality.LocalClusteringCoefficient(G, True).run().scores()
					cc = sum(lcc) / n
					print("c",end='',flush=True)
					degen = max(networkit.centrality.CoreDecomposition(G).run().scores())
					print("k",end='',flush=True)
					degs = networkit.centrality.DegreeCentrality(G).run().scores()
					degAss = networkit.correlation.Assortativity(G, degs).run().getCoefficient()
					print("a",end='',flush=True)
					comp = networkit.components.ConnectedComponents(G).run()
					numComp = comp.numberOfComponents()
					sizeOfLargestComp = max(comp.getComponentSizes())
					print("p")

					f.write("\t".join([str(item) for item in [T, gamma, k, 2*m/n, diam, cc, degen, degAss, numComp, sizeOfLargestComp]]) + "\n")
					f.flush()
				
					print("Finished run in ", end - start, " seconds, resulting in ", G.numberOfEdges(), " edges. AvgDegree: ", 2*m / n)
