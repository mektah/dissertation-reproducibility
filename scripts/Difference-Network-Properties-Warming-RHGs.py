import pandas
import os
import numpy as np

def differences(nativeDF, bandDF):
	colNames = ["avgDegree", "Diameter", "cc", "degen", "degAss"]
	renameMap = {colname+'-stddev' : colname for colname in colNames}
	deviationNames = [colname+'-stddev' for colname in colNames]

	diff = bandDF - nativeDF
	subset = diff[colNames]

	varianceNative = np.power(nativeDF[deviationNames], 2)

	varianceBand = np.power(bandDF[deviationNames], 2)

	pooledVariance = varianceNative + varianceBand
	pooledStd = np.sqrt(pooledVariance)

	renamedStd = pooledStd.rename(columns=renameMap)

	weightedDiff = subset.apply(abs) / renamedStd

	indexCols = bandDF[['T', 'gamma', 'k']]

	normalizedDiff = pandas.concat([indexCols, weightedDiff],axis=1)

	rawDiff = pandas.concat([indexCols, subset],axis=1)

	relativeDiff = pandas.concat([indexCols, (subset / nativeDF[colNames]).apply(abs)],axis=1)

	return normalizedDiff, rawDiff, relativeDiff

if __name__ == "__main__":
	nativeFile = "native-stats.dat"
	bandFile = "band-stats.dat"

	nativeDF = pandas.read_csv(nativeFile, sep="\t")

	bandDF = pandas.read_csv(bandFile, sep="\t")

	normalizedDiff, rawDiff, relativeDiff = differences(nativeDF, bandDF)

	normalizedDiff.to_csv("diffOverStd.dat",index=False,sep='\t',na_rep=0)

	rawDiff.to_csv("diff.dat",index=False,sep='\t',na_rep=0)

	relativeDiff.to_csv("relativeDiff.dat",index=False,sep='\t',na_rep=0)