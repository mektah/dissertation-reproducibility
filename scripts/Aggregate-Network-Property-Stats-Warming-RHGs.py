from scipy import stats

import numpy as np

def writeOutput(filename, measurements, outColNames=None, numColumns=7):
    if outColNames == None:
        colNames = ["avgDegree", "Diameter", "cc", "degen", "degAss"]
        outColNames = []
        for name in colNames:
            outColNames.append(name)
            outColNames.append(name+"-stddev")

    with open(filename, "w") as f:
        f.write("\t".join(["T","gamma","k"]+outColNames)+"\n")
        
        for key in sorted(measurements.keys()):
            results = measurements[key]

            print(key, len(results))
            f.write("\t".join([str(keyelem) for keyelem in key]))
            for col in range(numColumns-2):                
                colResults = [row[col] for row in results]
                f.write("\t"+str(np.mean(colResults)) + "\t" + str(np.std(colResults)))
            f.write("\n")


def readFile(filename):
    result = dict()
    targetNumCols = 7
    with open(filename, 'r') as f:
        for line in f:
            elems = line.split()
            floats = [float(elem) for elem in elems]
            if not len(floats)-3 == targetNumCols:
                print("Malformed line." + line)
                continue
            
            keytuple = (floats[0], floats[1], floats[2])
            if not keytuple in result:
                result[keytuple] = []
                
            result[keytuple].append(floats[3:])
    return result

if __name__ == "__main__":
    nativeFile = "properties-native-repetitions.dat"
    compFile = "properties-bandgen-repetitions.dat"

    nativeDict  = readFile(nativeFile)
    compDict = readFile(compFile)

    writeOutput("native-stats.dat", nativeDict)
    writeOutput("band-stats.dat", compDict)