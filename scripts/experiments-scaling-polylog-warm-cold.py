import time
import networkit
import os
import psutil
from multiprocessing import cpu_count
import math
import subprocess


trialcount = 10
cpucount = cpu_count()

gamma = 3
k = 8
TList = [0,0.5]
nList = [10**4, 10**7]

filename = 'polylog-scaling.dat'

with open(filename, 'w') as f:
	f.write('% NetworKit version: ' + str(networkit.__version__) + '\n')
	f.write('% Gamma:' + str(gamma) + ' k:' + str(k) + '\n')
	f.write('n\tT\tp\ttime\n')

for n in nList:
	for i in range(trialcount):
		for th in range(1,cpucount+1):
			networkit.setNumberOfThreads(th)
			for T in TList:
				with open(filename, 'a') as f:
					print("Starting run with n: ", n, ", p: ", th, ", T:", T)
					start = time.time()
					G = networkit.generators.HyperbolicGenerator(n, k, gamma, T).generate()
					end = time.time()
					f.write(str(n)  + '\t' + str(T) + '\t' + str(th) + '\t' + str(end-start) + '\n')

					print("Finished run in ", end - start, " seconds. Average degree ", int(2*G.numberOfEdges() / G.numberOfNodes()) )
