#! /usr/bin/env python3.4

import time
import subprocess

def readHGGraph(path):
    lines = []
    with open(path, 'r') as f:
        for line in f:
            lines.append(line)

    paramlist = lines[0].split('\t')
    n = int(paramlist[1])

    G = Graph(n)

    for i in range(n+1,len(lines)):
        edgelist = lines[i].split('\t')
        firstNode = int(edgelist[0])-1
        secondNode = int(edgelist[1])-1
        assert(firstNode >= 0)
        assert(firstNode < n)
        assert(secondNode >= 0)
        assert(secondNode < n)
        G.addEdge(firstNode, secondNode)
    
    return G

def generateHGGraph(n=1000, k=10, gamma=2, T=0, seed=1, binary='hyperbolic_graph_generator'):
    subprocess.call([binary, '-n', str(n), '-g', str(gamma), '-k', str(k), '-t', str(T), '-s', str(seed)])
    subprocess.call(['rm', 'graph.hg'])


nList = [2**i for i in range(10, 19)]
gamma = 3
k = 8
T = 0.5

filename = 'aldecoa-lukewarm-time.dat'

with open(filename, 'w') as f:
	f.write('% T:' + str(T) + ' gamma:' + str(gamma) + ' k:' + str(k) + '\n')
	f.write('n\ttime\n')

with open(filename, 'a') as f:
	for n in nList:
		print("Starting run with", n, " nodes, gamma ", gamma, ", k ", k, " and T ", T)
		start = time.time()
		generateHGGraph(n, k, gamma, T, start, 'install/bin/hyperbolic_graph_generator')
		end = time.time()
		f.write(str(n)  + '\t' + str(end-start) + '\n')


	print("Finished run in ", end - start, " seconds.")
