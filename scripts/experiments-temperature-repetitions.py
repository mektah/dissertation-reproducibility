#! /usr/bin/env python3.4

import networkit
import time

n = 10**4
repetitions = 250
gammalist = [float(x)/10 for x in range(22, 60, 6)]
gammalist.reverse()
klist = [2**x for x in range(2,9,2)]
Tlist = [0] #[float(x)/10 for x in range(0,10,3)]
nlist = [10**x for x in range(4,5)]

with open("properties-bandgen-repetitions.dat", 'a') as f:
	for i in range(repetitions):
		for T in Tlist:
			for gamma in gammalist:
				for k in klist:
					print("Starting run with", n, " nodes, gamma ", gamma, ", k ", k, " and T ", T)
					start = time.time()
					G = networkit.generators.HyperbolicGenerator(n,k,gamma,T).generate()
					end = time.time()
					print("g",end='',flush=True)
					
					m = G.numberOfEdges()
					diam = networkit.distance.Diameter(G).run().getDiameter()[0]
					print("d",end='',flush=True)
					lcc = networkit.centrality.LocalClusteringCoefficient(G, True).run().scores()
					cc = sum(lcc) / n
					print("c",end='',flush=True)
					degen = max(networkit.centrality.CoreDecomposition(G).run().scores())
					print("k",end='',flush=True)
					degs = networkit.centrality.DegreeCentrality(G).run().scores()
					degAss = networkit.correlation.Assortativity(G, degs).run().getCoefficient()
					print("a",end='',flush=True)
					comp = networkit.components.ConnectedComponents(G).run()
					numComp = comp.numberOfComponents()
					sizeOfLargestComp = max(comp.getComponentSizes())
					print("p")

					f.write("\t".join([str(item) for item in [T, gamma, k, 2*m/n, diam, cc, degen, degAss, numComp, sizeOfLargestComp]]) + "\n")
					f.flush()
				
					print("Finished run in ", end - start, " seconds, resulting in ", G.numberOfEdges(), " edges. AvgDegree: ", 2*m / n)
