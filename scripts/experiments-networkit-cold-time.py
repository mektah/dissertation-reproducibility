import time
import networkit

print("Using NetworKit version " + networkit.__version__)

p = 1
networkit.setNumberOfThreads(p)

nList = [2**i for i in range(10, 30)]
gamma = 3
k = 8
T = 0

filename = 'polylog-cold-time.dat'

with open(filename, 'w') as f:
	f.write('% T:0 gamma:' + str(gamma) + ' k:' + str(k) + '\n')
	f.write('% Threads:' + str(p) + '\n')
	f.write('n\ttime\n')

for n in nList:
	with open(filename, 'a') as f:
		print("Starting run with", n, " nodes, gamma ", gamma, ", k ", k, " and T ", T)
		start = time.time()
		G = networkit.generators.HyperbolicGenerator(n, k, gamma).generate()
		end = time.time()
		f.write(str(n)  + '\t' + str(end-start) + '\n')

		print("Finished run in ", end - start, " seconds. Average degree ", int(G.numberOfEdges() / G.numberOfNodes()) )