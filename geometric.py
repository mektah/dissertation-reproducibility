import math

def hyperbolicAreaToRadius(area):
    return math.acosh(area/(2*math.pi)+1)

def getExpectedDegree(n, alpha, R):
    gamma = 2*alpha+1
    xi = (gamma-1)/(gamma-2)
    firstSumTerm = math.exp(-R/2)
    secondSumTerm = math.exp(-alpha*R)*(alpha*(R/2)*((math.pi/4)*pow((1/alpha),2)-(math.pi-1)*(1/alpha)+(math.pi-2))-1)
    expectedDegree = (2/math.pi)*xi*xi*n*(firstSumTerm + secondSumTerm)
    return expectedDegree

def searchTargetRadiusForColdGraphs(n, k, alpha, epsilon):
    gamma = 2*alpha+1
    xiInv = ((gamma-2)/(gamma-1))
    v = k * (math.pi/2)*xiInv*xiInv
    currentR = 2*math.log(n / v)
    lowerBound = currentR/2
    upperBound = currentR*2
    assert(getExpectedDegree(n, alpha, lowerBound) > k)
    assert(getExpectedDegree(n, alpha, upperBound) < k)
    radiusFound = False
    while not radiusFound:
        currentR = (lowerBound + upperBound)/2
        currentK = getExpectedDegree(n, alpha, currentR)
        if currentK < k:
            upperBound = currentR
        else:
            lowerBound = currentR
            
        radiusFound = abs(getExpectedDegree(n, alpha, currentR) - k) < epsilon
    
    return currentR

def getWarmTargetRadius(n, k, alpha, T):
    assert(T > 0)
    beta = 1/T
    gamma = 2*alpha + 1

    if T > 1:
        vdenom = ( (2/math.pi)**beta)*((gamma-1)/(gamma-2))**2
        v = k*(1-beta)/ vdenom
        R = 2*T*math.log(n/v)
    else:
        xiInv = ((gamma-2)/(gamma-1))
        Iinv = ((beta/math.pi)*math.sin(math.pi/beta))
        v = (k*Iinv)*(math.pi/2)*xiInv*xiInv
        R = 2*math.log(n / v)
    return R

def getC(n, k, alpha, T=0, epsilon=0.01):
    if T == 0:
        targetRadius = searchTargetRadiusForColdGraphs(n,k,alpha,epsilon)
    else:
        targetRadius = getWarmTargetRadius(n, k, alpha, T)
    normalRadius = 2*math.log(n)
    return targetRadius - normalRadius
