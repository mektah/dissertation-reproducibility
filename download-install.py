import subprocess
import os
import shutil
import sys

########################################################################
########################## Check Python version ########################
########################################################################

if sys.version_info <= (3, 5):
	sys.exit("At least Python 3.5 is needed.")
	

########################################################################
###################### Install package dependencies ####################
########################################################################

latexpackages = ['texlive-latex-base', 'texlive-pictures', 'texlive-latex-extra']
packagelist = ['git', 'g++', 'libgoogle-glog-dev', 'libatlas-base-dev', 'scons', 'libgtest-dev', 'autoconf', 'make'] + latexpackages
packagemanager = ['apt', 'install', '-y']

result = 1

try:
    result = subprocess.run(packagemanager + packagelist).returncode
except:
    pass

if result != 0:
    print("Installation failed, please ensure that the following packages are present:"+str(packagelist))

result = 1
try:
	result = subprocess.run(packagemanager + ['libgsl-dev']).returncode
except:
	pass

if result != 0:
	try:
		result = subprocess.run(packagemanager + ['libgsl0-dev']).returncode
	except:
		pass

if result != 0:
	print("Could not install either libgsl-dev or libgsl0-dev, please ensure that one of them is present.")



##################################################################
##################### Clone code Repositories ####################
##################################################################
def cloneRepo(repoName, command='git', options=[]):
    result = subprocess.run([command, 'clone']+options+[repoName]).returncode
    if result != 0:
        raise RuntimeError("Cloning of " + repoName + " failed.")
    return result

# Implementation of Blaesius et al.
try:
	os.chdir('girg-benchmark')
except OSError as e:
	cloneRepo('https://bitbucket.org/mektah/girg-benchmark.git')
	os.chdir('girg-benchmark')

subprocess.run(['git', 'checkout', 'dynamic-model'])
subprocess.run(['make'])
shutil.copy('../geometric.py', '.')
os.chdir('..')

#Implementation of Aldecoa et Orsini
try:
    os.chdir('Hyperbolic-Graph-Generator')
except OSError as e:
    cloneRepo('https://github.com/named-data/Hyperbolic-Graph-Generator.git')
    os.chdir('Hyperbolic-Graph-Generator')

subprocess.run(['git', 'checkout', 'edb4cf2'])

try:
    os.mkdir('install')
except FileExistsError as e:
    pass

installDirString = os.path.join(os.getcwd(), 'install')
subprocess.run(['./bootstrap.sh'])
subprocess.run(['./configure', '--prefix='+installDirString])
subprocess.run(['make'])
subprocess.run(['make', 'install'])
os.chdir('..')

# Googletest needs to be compiled with the same compiler, so we clone and compile it separately
try:
	os.chdir('googletest/googletest/make')
except OSError as e:
	cloneRepo('https://github.com/google/googletest.git')
	os.chdir('googletest/googletest/make')

subprocess.run(['make'])
subprocess.run(['ar', 'rv', 'libgtest.a', 'gtest-all.o'])
os.chdir('../../..')

# NetworKit-hyperbolic-kd
try:
	cloneRepo('https://github.com/kit-parco/networkit-hyperbolic-kd.git')
except RuntimeError as e:
	print("Cloning failed, assuming repository is already present.")

networkitDir = "networkit-hyperbolic-kd/"
shutil.copyfile("build.conf", networkitDir+"build.conf")
os.chdir(networkitDir)
subprocess.run(['git', 'checkout', 'Dev'])
subprocess.run(['scons', '--optimize=Opt', '--target=Tests'])
subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'])
os.chdir('..')

# NetworKit vanilla
try:
    cloneRepo('https://github.com/kit-parco/networkit.git')
except RuntimeError as e:
    print("Cloning failed, assuming repository is already present.")

vanillaDir = "networkit/"
shutil.copyfile("build.conf", networkitDir+"build.conf")
os.chdir(vanillaDir)
subprocess.run(['git', 'checkout', '0a8ccbd25b95796c5ae964b7d0d45b380479a957'])
subprocess.run(['git', 'submodule', 'update', '--init'])
subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'])
os.chdir('..')

#NetworKit legacy
try:
    cloneRepo('https://mektah@bitbucket.org/mektah/networkit-hyperbolic-legacy', 'hg', ['-y', '--insecure'])
except RuntimeError as e:
    print("Cloning failed, assuming repository is already present.")

legacyDir = "networkit-hyperbolic-legacy/"
shutil.copyfile("build.conf", legacyDir+"build.conf")
os.chdir(legacyDir)
subprocess.run(['hg', 'checkout', 'Dev'])
subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'])
os.chdir('..')

#NetworKit general polylog
try:
    cloneRepo('https://github.com/mlooz/networkit-general-polylog.git')
except RuntimeError as e:
    print("Cloning failed, assuming repository is already present.")

generalPolylogDir = "networkit-general-polylog/"
shutil.copyfile("build.conf", generalPolylogDir+"build.conf")
os.chdir(generalPolylogDir)
subprocess.run(['git', 'checkout', 'general-polylog'])
subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'])
os.chdir('..')
